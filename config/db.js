import mongoose from 'mongoose';
import config from 'config';

const db = config.get('mongoURI');

const mongoDB = async () => {
    try {
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useCreateIndex: true
        });
        console.log("mongoDB connnected");
    } catch (err) {
        console.log(err.message);
        process.exit(1);
    }
}

module.exports = mongoDB;