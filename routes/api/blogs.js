import express from 'express';
import Blog from '../../models/Blog';

const router = express.Router();


router.get('/', async (req,res) => {
    const blog = await Blog.find({});

    try {
        if(blog.length === 0){
            res.json({err: 'No blog Exist'})
        }else{
            res.json(blog)
        }
        
    } catch (error) {
        res.json({Error: `Error is ${error}`})
    }
});

router.post('/', async (req,res)=>{
    const {title, image, description} = req.body;

    const newBlog = new Blog ({
        title,
        image,
        description
    })

    try {
        const blog = await newBlog.save();
        res.json(blog)
    } catch (error) {
        res.json({Error: `Error is ${error}`})
    }

})

router.get('/:id', async (req,res) => {

    try {
        const blog = await Blog.findById(req.params.id);
        res.json(blog)
    } catch (error) {
        res.json({Error: `Error is ${error}`})
    }
});

router.delete('/:id', async (req,res) => {

    try {
        const blog = await Blog.findByIdAndDelete({_id:req.params.id});
        res.json({Success: 'Blog Deleted Successfully'})
    } catch (error) {
        res.json({Error: `Error is ${error}`})
    }
});

router.put('/:id', async (req,res) => {

    try {
        const blog = await Blog.findByIdAndUpdate({_id:req.params.id}, req.body);
        res.json({Success: 'Blog Updated Succesfully'})
    } catch (error) {
        res.json({Error: `Error is ${error}`})
    }
});


module.exports = router;