import express from 'express';
import auth from '../../middleware/auth';
import User from '../../models/User';
import config from 'config';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';


const { check, validationResult } = require('express-validator/check');
const router = express.Router();


router.get('/', auth, async(req,res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json(user);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Server Error');
    }
});



router.post('/',[
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password is required').exists()
],

async (req,res) =>{
    const error = validationResult(req);
    if(!error.isEmpty()){
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const {email, password} = req.body;

    try {
    // User Exist
    let user = await User.findOne({email});

    if(!user) {
     return res
        .status(400)
        .json({errors: [{msg: 'Invalid Creadantials!'}]});
    }

    // Match Email and Password

    const isMatch = await bcrypt.compare(password, user.password);

    if(!isMatch) {
        return res
           .status(400)
           .json({errors: [{msg: 'Invalid Creadantials!'}]});
       }

    // Return jsonwebtoken
    const payload = {
        user: {
            id: user.id
        }
    }

    jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: 360000 },
        (err, token) => {
            if(err) throw err;
            res.json({token});
        }
        );

    } catch (err) {
        console.log(err.message);
        res.status(500).send('Server Error');
    }
});

module.exports = router;