import express from 'express';
import gravatar from 'gravatar';
import bcrypt from 'bcryptjs';
import User from '../../models/User';
import jwt from 'jsonwebtoken';
import config from 'config';


const router = express.Router();
const { check, validationResult } = require('express-validator/check');

router.post('/',[
    check('name', 'Name is required')
        .not()
        .isEmpty(),
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Please enter a password with 6 or more character').isLength({min: 6,})
],

async (req,res) =>{
    const error = validationResult(req);
    if(!error.isEmpty()){
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const {name, email, password} = req.body;

    try {
    // User Exist
    let user = await User.findOne({email});

    if(user) {
     return res
        .status(400)
        .json({errors: [{msg: 'User already exists !'}]});
    }

    //Get User Gravator
    const avatar = gravatar.url(email, {
        s: '200',
        r: 'pg',
        d: 'mm'
    })

    user = new User({
        name,
        email,
        avatar,
        password
    });

    //Excrypt Password

    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);

    await user.save();

    // Return jsonwebtoken
    const payload = {
        user: {
            id: user.id
        }
    }

    jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: 360000 },
        (err, token) => {
            if(err) throw err;
            res.json({token});
        }
        );

    } catch (err) {
        console.log(err.message);
        res.status(500).send('Server Error');
    }
});

module.exports = router;