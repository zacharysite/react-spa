import jwt from 'jsonwebtoken';
import config from 'config';






module.exports = (req, res, next) => {
    // Get Token From Header

    const token = req.header('x-auth-token');

    if(!token){
        return res.status(401).json({msg: 'No token, authorizato denied'});
    }


    // Verify Token

    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));
        req.user = decoded.user;
        next();
    } catch (error) {
        res.status(401).json({msg: 'Token is not valid'});
    }
}