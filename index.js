import express from 'express';
// import models from './models';
import bodyParser from 'body-parser';
import mongoDB from './config/db';

// ROUTES IMPORT
import users from './routes/api/users';
import blogs from './routes/api/blogs';
import auth from './routes/api/auth';
import profile from './routes/api/profile';

const app = express();
mongoDB();

app.get('/', (req, res) => res.send("API running"));


//BODY-PARSER middleware
app.use(bodyParser.json({extended: false}));



//routes
app.use('/api/users', users);
app.use('/api/blogs', blogs);
app.use('/api/auth', auth);
app.use('/api/profile', profile);

const PORT = process.env.PORT || 8090;

app.listen(PORT, ()=> console.log("Server is running!"));