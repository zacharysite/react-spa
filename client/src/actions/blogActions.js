import axios from 'axios';
import { FETCH_BLOGS, SINGLE_BLOG, ADD_BLOGS, UPDATE_BLOG, DELETE_BLOG } from './types';


export const fetchBlogs = () => dispatch => {
    axios.get('/api/blogs')
        .then(res => dispatch({
            type: FETCH_BLOGS,
            payload: res.data
        }))
}

export const fetchSingleBlog = id => dispatch => {
    console.log('actions')
    axios.get(`/api/blogs/${id}`)
        .then(res => dispatch({
            type: SINGLE_BLOG,
            payload: res.data
        }))
}

export const addBlog = newBlog => dispatch => {
    console.log('add actions')
    axios.post('/api/blogs', newBlog)
        .then(res => dispatch({
            type: ADD_BLOGS,
            payload: res.data
        }))
}

export const editBlog = (id, newBlog) => dispatch => {
    console.log('edit actions')
    axios.put(`/api/blogs/${id}`, newBlog)
        .then(res => dispatch({
          type: UPDATE_BLOG,
          payload: res.data
        }))
  }
  
  export const deleteBlog = id => dispatch => {
    console.log('delete actions')
    axios.delete(`/api/blogs/${id}`)
        .then(res => dispatch({
          type: DELETE_BLOG,
          payload: id
        }))
  }