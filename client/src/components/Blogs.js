import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import {fetchBlogs} from '../actions/blogActions';


class Blogs extends Component {

  componentDidMount(){
    this.props.fetchBlogs();
}

  render() {

    const {blogs} = this.props
    let blogData

    if(blogs.length === 0){
      blogData = <h1>Loading...</h1>
    }else{
      blogData = blogs.map(blog => (
        <div className="ui divided items">
        <div className="item" key={blog._id}>
          <div className="image">
            <img src={blog.image} alt="blog image" />
          </div>
          <div className="content">
            <a className="header">{blog.title}</a>
           <div className="meta">
            <span>22/12/19</span>
           </div>
            <div className="description">
              <p>{blog.description}</p>
            </div>
            <div className="extra">
              <Link to={`blogs/${blog._id}`} className="ui floated basic violet button">Read More <i className="right chevron icon"></i></Link>
            </div>
          </div>
        </div>
        <hr/>
      </div>
      ))
    }
    return (
      <div className="ui main text container">
        <div className="ui huge header"></div>
        <div className="ui top attached segment">
            {blogData}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state =>({
  blogs: state.blogs.blogs
})

export default connect(mapStateToProps, {fetchBlogs})(Blogs)