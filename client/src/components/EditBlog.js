import React, { Component } from 'react'
import {connect} from 'react-redux';
import { editBlog } from '../actions/blogActions';

class EditBlog extends Component {
  
  constructor(props){
    super(props)
    const {blog} = this.props
    this.state = {
       title: blog.title,
       imgae: blog.image,
       description: blog.description
    }
  }

  onChange = (e) => {
    this.setState({ 
      [e.target.name] : e.target.value 
    })
  }
  
  onSubmit = (e) => {
    e.preventDefault()

    const newBlog = {
      title: this.state.title,
      image: this.state.image,
      description: this.state.description
    }

    this.props.editBlog(this.props.match.params.id, newBlog);
    this.props.history.push('/')
  }
  
  render() {
    return (
      <div className="ui main text container segement">
      <div className="ui huge header">Edit Blog</div>

      <form className="ui form" onSubmit={this.onSubmit}>
        <div className="field">
          <label>Title</label>
          <input type="text" onChange={this.onChange} value={this.state.title} name="title" placeholder="Title"/>
        </div>

        <div className="field">
          <label>Image</label>
          <input type="text" onChange={this.onChange} value={this.state.image} name="image" placeholder="Image"/>
        </div>


        <div className="field">
          <label>Description</label>
          <textarea placeholder="Description" onChange={this.onChange} value={this.state.description} name="description"></textarea>
        </div>

        <button className="ui inverted violet big button">Update</button>
      
      </form>

        
      </div>
    )
  }
}

const mapStateToProps = state => ({
  blog: state.blogs.blog
})

export default connect(mapStateToProps,{editBlog})(EditBlog)
