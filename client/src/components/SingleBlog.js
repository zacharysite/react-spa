import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import { fetchSingleBlog,deleteBlog,fetchBlogs } from '../actions/blogActions';


class SingleBlog extends Component {
  componentDidMount(){
    this.props.fetchSingleBlog(this.props.match.params.id);
  }

  onClick = (id) =>{
    this.props.deleteBlog(id)
    this.props.history.push('/')
    this.props.fetchSingleBlog()
  }


  render() {
    const {blog} = this.props
    return (
        <div className="ui main text container segment">
        <div className="ui huge header">{blog.title}</div>
        <hr></hr>
        <div className="ui top attached">
          <div className="item">
            <img className="ui centered rounded image" src={blog.image} alt="hsgdjgs" />
            <div className="content"><span>7/7/19</span></div>
            <hr></hr>
            <div className="description">
              <p>{blog.description}</p>
            </div>
  
            <hr></hr>
            <div>
  
              <button className="ui inverted orange button"><Link to={`/blogs/${blog._id}/edit`}>Edit</Link></button>
              <button className="ui inverted red button" onClick={()=> this.onClick(blog._id)}>Delete</button>
  
            </div>
          </div>
        </div>
          
        </div>
    )
  }
}

const mapStateToProps = state => ({
  blog: state.blogs.blog
})

export default connect(mapStateToProps, {fetchSingleBlog, deleteBlog, fetchBlogs})(SingleBlog)