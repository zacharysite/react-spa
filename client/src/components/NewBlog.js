import React, { Component } from 'react';
import {connect} from 'react-redux';
import { addBlog } from '../actions/blogActions';

class NewBlog extends Component {
  constructor(props){
    super(props)
    this.state = {
       title: '',
       imgae: '',
       description: ''
    }
  }
  
  onChange = (e) => {
    this.setState({ 
      [e.target.name] : e.target.value 
    })
  }
  
  onSubmit = (e) => {
    e.preventDefault()

    const newBlog = {

      title: this.state.title,
      image: this.state.image,
      description: this.state.description
    }

    this.props.addBlog(newBlog);
    this.props.history.push('/')
  }
  
  
  render() {
    return (
        <div className="ui main text container segement">
        <div className="ui huge header">New Blog</div>
  
        <form className="ui form" onSubmit={this.onSubmit}>
          <div className="field">
            <label>Title</label>
            <input type="text" onChange={this.onChange} value={this.state.title} name="title" placeholder="Title" />
          </div>
  
          <div className="field">
            <label>Image</label>
            <input type="text" onChange={this.onChange} value={this.state.image} name="image" placeholder="Image" />
          </div>
  
  
          <div className="field">
            <label>Description</label>
            <textarea placeholder="Description" onChange={this.onChange} value={this.state.description} name="description" />
          </div>
  
          <button className="ui inverted violet big button">Submit</button>
        
        </form>
  
          
        </div>
    )
  }
}

export default connect(null, {addBlog})(NewBlog)
