import {FETCH_BLOGS, SINGLE_BLOG, ADD_BLOGS, DELETE_BLOG } from '../actions/types';

const inisitalState = {
    blogs: [],
    blog: {}   
}

export default (state = inisitalState, action) => {
    switch(action.type){
        case FETCH_BLOGS:
            return{
                ...state,
                blogs: action.payload
            }
        case SINGLE_BLOG:
            return{
                ...state,
                blog: action.payload
            }
        case ADD_BLOGS:
            return{
                ...state,
                blog: action.payload
            }
        case DELETE_BLOG:
            return{
              ...state,
              blog: state.blogs.filter(blog => blog._id !== action.payload)
            }

        default:
            return state
    }
}