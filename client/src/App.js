import React, {Component, Fragment} from 'react';
import './App.css';
import './style/style.css';
// import Navbar from './components/layout/Navbar';
import Home from './components/Home';
import Blogs from './components/Blogs';
import Contact from './components/Contact';
import AboutUs from './components/AboutUs';
// import NewBlog from './components/NewBlog';
import EditBlog from './components/EditBlog';
import SingleBlog from './components/SingleBlog';

import {BrowserRouter as Router, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
              {/* <Navbar /> */}
              <Route exact path="/" component={Home} />
              <Route exact path="/aboutus" component={AboutUs} />
              <Route exact path="/contact" component={Contact} />
              <Route exact path="/blogs" component={Blogs} />
              <Route exact path="/blogs/:id" component={SingleBlog} />
              <Route exact path="/blogs/:id/edit" component={EditBlog} />
          </div>
        </Router>
      </Provider>
    );
  }
}
